﻿#Qiskit

Qiskit is an open-source framework for working with noisy intermediate-scale quantum computers. Here I give a simpler version of Qiskit tutorial based on [qiskit tutorial](https://github.com/Qiskit/qiskit-tutorials) and [Installation](https://github.com/Qiskit/qiskit-tutorials/blob/master/INSTALL.md).

## Requirements
Qiskit supports Python 3.5 or later. Jupyter Notebook is recommended for interacting with Qiskit.

Qiskit is tested and supported on the following 64-bit systems:

-Ubuntu 16.04 or later
-macOS 10.12.6 or later
-Windows 7 or later

## Installation
Please follow the following steps to install the environment:

1. Install Anaconda 3.

2. Create a new environment.
```
conda activate env_name
```
Note that `env_name` is the name you given to this new environment.

3. Activate the newly generated environment.
```
conda activate env_name
```

4. Install QISKit package.
```
pip install qiskit
```

5. Install Jupyter Notebook.
```
pip install jupyter
```

6. Run Jupyter Notebook.
```
jupyter notebook
```
After you've installed, import Qiskit package into your environment with Python and enjoy the quantum programming.
```
import qiskit
```

## Access to IBM Q Devices
1. Create an IBM Q account if you do not have one.

2. Go to `My Account` > `Advanced` > `API Token` to get your token.

3. Copy your token and paste it below:
```
My_token = 'XXX' # Paste your token here
from qiskit import IBMQ
IBMQ.save_account(My_token) # Store API credentials locally
```

## Checking and Updating
1. Check all the Qiskit elements installed in your environment.
```
import qiskit
qiskit.__qiskit_version__
```

2. update the latest version of Qiskit
```
pip install --upgrade qiskit
```

## How to Perform Quantum Operators
Tutorials for [Qiskit Terra](https://github.com/Qiskit/qiskit-tutorials/tree/master/qiskit/terra).

